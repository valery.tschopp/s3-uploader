FROM ruby:alpine

LABEL maintainer="valery.tschopp@switch.ch"

#RUN gem install sinatra shotgun haml aws-sdk
COPY Gemfile /
RUN bundle install

RUN mkdir -p /app/views /app/public
COPY /views /app/views
COPY /public /app/public
COPY /s3uploader.rb /app/

EXPOSE 8080
ENTRYPOINT ["shotgun", "--host", "0.0.0.0", "--port", "8080", "/app/s3uploader.rb"]
