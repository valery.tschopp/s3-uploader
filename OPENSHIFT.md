# Deploy on OpenShift

## Login

```
oc login https://console.zh.shift.switchengines.ch --username=$OS_USERNAME --password=$OS_PASSWORD
```

## Create a project

```
oc new-project vt
```

## Create application and build

Edit the `s3uploader.env` with the correct S3 variables:

* `AWS_ACCESS_KEY_ID`: AWS/EC2 access key
* `AWS_SECRET_ACCESS_KEY`: AWS/EC2 secret key
* `S3_ENDPOINT`: S3 endpoint (e.g. `https://os.zhdk.cloud.switch.ch`)
* `S3_BUCKET_NAME`: S3 bucket name, **THE BUCKET MUST EXISTS**

```
# oc new-app --env-file=s3uploader.env https://gitlab.switch.ch/valery.tschopp/s3-uploader
oc new-app --env-file=s3uploader.env .

oc logs -f bc/s3-uploader
```

## Expose application

With HTTPS:

```
oc create route edge --service s3-uploader --insecure-policy Redirect
```

## Rebuild image

Edit the code, git commit, git push, and then:

```
oc start-build s3-uploader --follow
```